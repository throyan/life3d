﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	public int size;
	public bool lifeMode = false;
	public float delay = 1; // seconds
	public float cellDistance = 1.2f;
	public Cell cellPrefab;
	public bool selectionMode = false;

	private bool createIsInProgress = false;
	private float lastTime = 0;
	private Cell[,,] cells;
	private Vector3[] initializationVector = { new Vector3 (0, 0, 0), new Vector3 (-1, 0, 0), new Vector3 (1, 0, 0) };
	private int[] LifeRules = { 3, 4, 3, 2 };

	void Start()
	{
		this.CreateField (this.size);
	}

	void FixedUpdate()
	{
		if (!this.lifeMode || this.createIsInProgress) {
			return;
		}
		// separated this long if
		// check timestep
		if (this.lastTime != 0 && Time.fixedTime - this.lastTime < this.delay) {
			return;
		}
		this.lastTime = Time.fixedTime;

		// need some buffer to update cells
		List<Cell> cellsToUpdate = new List<Cell> ();
		foreach (Cell cell in this.cells) {
			if (cell.IsAlive != this.ToBeOrNotToBe (cell)) {
				cellsToUpdate.Add (cell);
			}
		}

		// actual update
		cellsToUpdate.ForEach ((Cell cell) => {
			cell.IsAlive = !cell.IsAlive;
		});
	}

	public void SetSelectionMode(bool mode)
	{
		this.selectionMode = mode;

		foreach (Cell cell in this.cells) { // how does it work?
			cell.IsSelectionMode = this.selectionMode;
		}
	}

	public bool CreateField(int size) {
		if (this.createIsInProgress) {
			return false;
		}

		this.createIsInProgress = true;
		this.size = size;

		if (this.cells != null) {
			// destroy all current cells
			foreach (Cell cell in this.cells) {
				Destroy (cell.gameObject); // is it okay?
			}
		}

		this.StartCoroutine ("CreateCells");

		return true;
	}

	private IEnumerator CreateCells()
	{
		this.cells = new Cell[this.size, this.size, this.size];
		Vector3 startPoint = this.transform.position - new Vector3 (1, 1, 1) * ((this.size - 1) / 2f * this.cellDistance);

		for (int x = 0; x < this.size; x++) {
			for (int y = 0; y < this.size; y++) {
				for (int z = 0; z < this.size; z++) {
					Vector3 vector = startPoint + (new Vector3 (x, y, z) * this.cellDistance);
					this.cells [x, y, z] = Instantiate (this.cellPrefab, vector, Quaternion.identity);
					this.cells [x, y, z].coords = new Vector3 (x, y, z);
					this.cells [x, y, z].IsSelectionMode = this.selectionMode;
				}
			}
			// create field by packs with size = this.size ^ 2
			yield return null;
		}

		// next tasks are not hard
		for (int x = 0; x < this.size; x++) {
			for (int y = 0; y < this.size; y++) {
				for (int z = 0; z < this.size; z++) {
					Cell cell = this.cells [x, y, z];
					cell.siblings = this.getSiblings (cell);
				}
			}
		}

		foreach (Vector3 coord in this.initializationVector) {
			Cell cell = this.getByRelativeCoords (coord);
			cell.IsAlive = true;
		}

		this.createIsInProgress = false;
	}

	public Cell getByRelativeCoords(Vector3 coords)
	{
		int min = (int)Math.Floor (this.size / 2f);
		return this.cells [(int) coords.x + min, (int) coords.y + min, (int) coords.z + min];
	}

	public void ChangeCellDistance(float distance)
	{
		this.cellDistance = distance;

		if (this.cells == null) {
			return;
		}

		Vector3 startPoint = this.transform.position - new Vector3 (1, 1, 1) * (this.size - 1) / 2f * this.cellDistance;

		foreach (Cell cell in this.cells) {
			Vector3 position = startPoint + (cell.coords * this.cellDistance);
			cell.transform.position = position;
		}
	}

	public void ChangeDelay(float delay)
	{
		this.delay = delay;
	}

	public void SetLifeRules(int r1, int r2, int r3, int r4)
	{
		this.LifeRules = new int[] { r1, r2, r3, r4 };
	}

	private bool ToBeOrNotToBe(Cell cell)
	{
		int sum = cell.aliveSiblingsCount;
		if (sum >= LifeRules [0] && sum <= LifeRules [1]) {
			return true;
		} else if (sum > LifeRules [2] || sum < LifeRules [3]) {
			return false;
		}

		// remain alive or dead (maybe insert this in conditions?)
		return cell.IsAlive;
	}

	private Cell[] getSiblings(Cell cell)
	{
		Cell[] siblings = new Cell[26];
		int i = 0;
		// I don't wont to write 26 lines
		for (int dX = -1; dX <= 1; dX++) {
			for (int dY = -1; dY <= 1; dY++) {
				for (int dZ = -1; dZ <= 1; dZ++) {
					// skip current cell
					if (dX == 0 && dY == 0 && dZ == 0) {
						continue;
					}

					siblings[i++] = this.cells[
						this.cycleCoordinate ((int) cell.coords.x + dX),
						this.cycleCoordinate ((int) cell.coords.y + dY),
						this.cycleCoordinate ((int) cell.coords.z + dZ)
					];
				}
			}
		}

		return siblings;
	}

	private int cycleCoordinate(int coord)
	{
		return (coord + this.size) % this.size;
	}
}
