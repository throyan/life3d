﻿using UnityEngine;
using System.Collections;

public class Cell : MonoBehaviour {

	public Vector3 coords;
	public Cell[] siblings { get; set; }

	public int aliveSiblingsCount { get; private set; }
	private Animator animator;

	public Cell()
	{
		// set default value (cause it's not C# 4.0...)
		this.aliveSiblingsCount = 0;
		//this.animator = this.GetComponent<Animator> ();
	}

	// todo: make it editable from editor. look for [SerializeField]
	public bool IsAlive {
		get { return this.animator.GetBool ("isAlive"); }
		set {
			// prevent set same value
			if (this.animator.GetBool ("isAlive") == value) {
				return;
			}

			this.animator.SetBool ("isAlive", value);

			// update sum for siblings
			foreach (Cell sibling in this.siblings) {
				sibling.aliveSiblingsCount += value ? 1 : -1;
			}
		}
	}

	public bool IsSelectionMode {
		get { return this.animator.GetBool ("selectionMode"); }
		set { this.animator.SetBool ("selectionMode", value); }
	}

	void Awake()
	{
		this.animator = this.GetComponent<Animator> ();
	}

	/// <summary>
	/// Click on cell
	/// </summary>
	void OnMouseDown()
	{
		if (this.IsSelectionMode) {
			this.IsAlive = !this.IsAlive;
		}
	}
}
