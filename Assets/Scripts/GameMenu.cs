﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour {

	public Text startGameButtonText;
	public GameManager gameManager;
	public Text helpText;
	public InputField sizeInput;
	public InputField rulesInput;

	public void StartGame()
	{
		this.gameManager.lifeMode = !this.gameManager.lifeMode;
		string text = this.gameManager.lifeMode
			? "Stop" : "Start";
		this.startGameButtonText.text = text;
	}

	public void ResetField(string text)
	{
		int size;
		int.TryParse (text, out size);

		if (size <= 0) {
			this.setInputColor (this.sizeInput, Color.red);

			return;
		}

		if (!this.gameManager.CreateField (size)) {
			this.setInputColor (this.sizeInput, Color.red);

			return;
		}
	}

	public void ResetSizeInputColor(string text)
	{
		this.setInputColor (this.sizeInput, Color.white);
	}

	public void ResetRulesInputColor(string text)
	{
		this.setInputColor (this.rulesInput, Color.white);
	}

	public void SetLifeRules(string text)
	{
		string[] parts = text.Split (' ');

		if (parts.Length != 4) {
			this.setInputColor (this.rulesInput, Color.red);

			return;
		}

		int r1, r2, r3, r4;
		int.TryParse (parts [0], out r1);
		int.TryParse (parts [1], out r2);
		int.TryParse (parts [2], out r3);
		int.TryParse (parts [3], out r4);
		if (r1 == 0 || r2 == 0 || r3 == 0 || r4 == 0) {
			this.setInputColor (this.rulesInput, Color.red);

			return;
		}

		this.gameManager.SetLifeRules (r1, r2, r3, r4);
	}

	public void Update()
	{
		if (Input.GetKeyDown (KeyCode.Space)) {
			this.StartGame ();
		}
		if (Input.GetKeyDown (KeyCode.H)) {
			this.helpText.enabled = !this.helpText.enabled;
		}
	}

	private void setInputColor(InputField input, Color color)
	{
		input.GetComponent<Image> ().color = color;
	}
}
