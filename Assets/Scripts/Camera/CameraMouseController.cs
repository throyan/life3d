﻿using UnityEngine;
using System.Collections;

// @link http://wiki.unity3d.com/index.php?title=MouseOrbitImproved
// @link http://wiki.unity3d.com/index.php/TouchLook
public class CameraMouseController : AbstractCameraController {
	public float xSpeed = 120.0f;
	public float ySpeed = 120.0f;

	public override Vector2 calcAngles(Vector2 angles, float distance) {
		// rotate with right mouse button
		if (Input.GetMouseButton (1)) {
			angles.x += Input.GetAxis ("Mouse X") * this.xSpeed * distance * 0.02f;
			angles.y -= Input.GetAxis ("Mouse Y") * this.ySpeed * distance * 0.02f;
		}

		return angles;
	}

	public override float calcDistance(float distance) {
		// scroll with mousewheel
		if (Input.GetAxis ("Mouse ScrollWheel") != 0) {
			distance -= Input.GetAxis ("Mouse ScrollWheel") * 5;
		}

		return distance;
	}

}