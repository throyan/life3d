﻿using System;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	public Transform target;
	public float distance = 15;

	public float yMinLimit = -20f;
	public float yMaxLimit = 80f;

	public float distanceMin = .5f;
	public float distanceMax = 15f;

	private Rigidbody rigidBody;
	private Camera cameraComponent;
	private AbstractCameraController[] controllers;
	private Vector2 angles;

	void Start()
	{
		this.controllers = this.GetComponents<AbstractCameraController> ();
		this.cameraComponent = this.GetComponent<Camera> ();

		this.rigidBody = GetComponent<Rigidbody>();

		// Make the rigid body not change rotation
		if (this.rigidBody != null)
		{
			this.rigidBody.freezeRotation = true;
		}

		Vector3 angles = this.transform.eulerAngles;
		this.angles = new Vector2 (angles.x, angles.y);
	}
	
	void LateUpdate () 
	{
		if (!this.target) {
			return;
		}

		foreach (AbstractCameraController controller in this.controllers) {
			this.angles = controller.calcAngles (this.angles, this.distance);
			this.distance = controller.calcDistance (this.distance);
		}

		// cut values
		this.angles.y = CameraController.ClampAngle (this.angles.y, this.yMinLimit, this.yMaxLimit);
		this.distance = Mathf.Clamp (this.distance, this.distanceMin, this.distanceMax);

		Quaternion rotation = Quaternion.Euler (this.angles.y, this.angles.x, 0);
		this.transform.rotation = rotation;
		Vector3 negDistance = new Vector3(0.0f, 0.0f, - this.distance);
		Vector3 position = rotation * negDistance + this.target.position;

		this.transform.position = position;

		this.cameraComponent.orthographicSize = this.distance / 2; // why 2?
	}

	public static float ClampAngle(float angle, float min, float max)
	{
		if (angle < -360F) {
			angle += 360F;
		}
		if (angle > 360F) {
			angle -= 360F;
		}
		return Mathf.Clamp(angle, min, max);
	}
}

