﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraKeyboardController : AbstractCameraController {

	public float speed = 5;
	public float rushSpeed = 20;

	public override Vector2 calcAngles(Vector2 angles, float distance) {
		// we do zoom on alt!
		if (Input.GetKey (KeyCode.LeftAlt)) {
			return angles;
		}

		float speed = this.getSpeed ();

		if (Input.GetKey (KeyCode.LeftArrow)) {
			angles.x -= speed;
		}
		if (Input.GetKey (KeyCode.RightArrow)) {
			angles.x += speed;
		}
		if (Input.GetKey (KeyCode.UpArrow)) {
			angles.y += speed;
		}
		if (Input.GetKey (KeyCode.DownArrow)) {
			angles.y -= speed;
		}

		return angles;
	}

	public override float calcDistance (float distance)
	{
		if (! Input.GetKey (KeyCode.LeftAlt)) {
			return distance;
		}
	
		float speed = this.getSpeed () / 5;

		if (Input.GetKey (KeyCode.UpArrow)) {
			distance -= speed;
		}
		if (Input.GetKey (KeyCode.DownArrow)) {
			distance += speed;
		}

		return distance;
	}

	private float getSpeed()
	{
		if (Input.GetKey (KeyCode.LeftShift)) {
			return this.rushSpeed;
		}

		return this.speed;
	}
}
