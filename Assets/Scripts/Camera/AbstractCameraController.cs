﻿using System;
using UnityEngine;

[RequireComponent(typeof(CameraController))]
public abstract class AbstractCameraController : MonoBehaviour // to attach to gameobject
{
	
	public abstract Vector2 calcAngles(Vector2 angles, float distance);
	public abstract float calcDistance(float distance);
}
