﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTouchController : AbstractCameraController {

	public float sensitivityX = 5.0f;
	public float sensitivityY = 5.0f;

	public bool invertX = false;
	public bool invertY = false;

	public override Vector2 calcAngles(Vector2 angles, float distance) {
		if (Input.touchCount > 0) {
			if (Input.touchCount == 1 && Input.touches [0].phase == TouchPhase.Moved) {
				Vector2 delta = Input.touches [0].deltaPosition;
				float rotationZ = delta.x * sensitivityX * Time.deltaTime;
				rotationZ = invertX ? rotationZ : rotationZ * -1;
				float rotationX = delta.y * sensitivityY * Time.deltaTime;
				rotationX = invertY ? rotationX : rotationX * -1;

				angles.x -= rotationZ;
				angles.y += rotationX;
			}
		}

		return angles;
	}

	public override float calcDistance(float distance) {
		// pinch
		// @link https://unity3d.com/ru/learn/tutorials/topics/mobile-touch/pinch-zoom
		if (Input.touchCount > 1) {
			// Store both touches.
			Touch touchZero = Input.GetTouch (0);
			Touch touchOne = Input.GetTouch (1);

			// Find the position in the previous frame of each touch.
			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

			// Find the magnitude of the vector (the distance) between the touches in each frame.
			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

			// Find the difference in the distances between each frame.
			float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

			// use speed
			distance += deltaMagnitudeDiff / 20;
		}

		return distance;
	}
}
